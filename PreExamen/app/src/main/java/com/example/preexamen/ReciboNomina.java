package com.example.preexamen;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasNormales;
    private int horasExtras;
    private int puesto;
    // 1 = Auxiliar, 2 = Albañil, 3 = Ingeniero
    private double porcentajeImpuesto;

    public ReciboNomina(int numRecibo, String nombre, int horasNormales, int horasExtras, int puesto, double porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public double calcularSubtotal() {
        double pagoBase = 200;
        double pagoPorHora = pagoBase;

        switch(puesto) {
            case 1:
                pagoPorHora += pagoBase * 0.20;
                break;
            case 2:
                pagoPorHora += pagoBase * 0.50;
                break;
            case 3:
                pagoPorHora += pagoBase * 1.00;
                break;
        }

        return (horasNormales * pagoPorHora) + (horasExtras * pagoPorHora * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * porcentajeImpuesto;
    }

    public double calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }
}
